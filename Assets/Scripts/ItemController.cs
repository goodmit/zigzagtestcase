﻿using UnityEngine;

/// <summary>
/// Базовый класс для различных предметов на уровне(не являющихся платформами Ground)
/// Позволяет взаимодействовать с игровыми объектами (на данный момент только с кристаллами)
/// </summary>
public class ItemController : MonoBehaviour, IInteractable, IDestroyable
{

    #region Public
    /// <summary>
    /// Взаимодействие с объектом. Виртуальный класс. Необходимо реализовывать в потомках.
    /// </summary>
    public virtual void Interact()
    {
        Debug.LogError("have to realize it through children!");
    }

    /// <summary>
    /// Удаляет объект со сцены
    /// </summary>
    public void KillMe()
    {
        if (gameObject != null)
        {
            Destroy(gameObject);
        }
    }
    #endregion

}
