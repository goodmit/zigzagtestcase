﻿
public interface IDestroyable
{
    void KillMe();
}
