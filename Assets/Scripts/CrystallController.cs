﻿using UnityEngine;

/// <summary>
/// Контроллер кристаллов.
/// Класс, отвечающий за поведение кристаллов на сцене
/// </summary>
public class CrystallController : ItemController
{
    
    [Tooltip("Шаг вращения")]
    [SerializeField]
    private Vector3 _rotationAngle = default;
    [Tooltip("Скорость вращения")]
    [SerializeField]
    private float _rotationSpeed = default;
    [Tooltip("Очки за сбор предмета")]
    [SerializeField]
    private int _collectScore = default;

    private static int _id = 0;

    void OnDestroy()
    {
        EventManager.Instance.Remove(GameEventType.GAME_OVER, OnKillMe);
    }

    void Awake()
    {
        _id++;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.name = "Crystall_" + _id;
        EventManager.Instance.Add(GameEventType.GAME_OVER, OnKillMe);
    }
    
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(_rotationAngle * _rotationSpeed * Time.deltaTime);
    }

    /// <summary>
    /// Взаимодействие с объектом Кристалл. Удаляет экземпляр кристалла со сцены и начисляет очки за его сбор
    /// </summary>
    override public void Interact()
    {
        ScoreController.Instance.AddScore(_collectScore);
        KillMe();
    }

    #region Private
    private void OnKillMe(object obj)
    {
        KillMe();
    }
    #endregion
}
