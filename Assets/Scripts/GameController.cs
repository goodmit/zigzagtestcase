﻿using UnityEngine;

/// <summary>
/// Основной игровой контроллер
/// Класс, управляющий игровым процессом(не игровым персонажем!!) в целом.
/// Связывает работу различных модулей.
/// </summary>
public class GameController : Singleton<GameController>
{
    [Header("Игровой персонаж")]
    [Tooltip("Префаб игрового персонажа")]
    [SerializeField]
    private GameObject _player = default;
    [Header("Камера")]
    [Tooltip("Основная камера на уровне")]
    [SerializeField]
    private Camera _mainCamera = default;
    [Tooltip("Стартовые координаты камеры")]
    [SerializeField]
    private Vector3 _startPosCamera = default;
    [Header("Игровое окружение")]
    [Tooltip("Префаб кристалла")]
    [SerializeField]
    private GameObject _itemPrefab = default;
    [Tooltip("Префаб, платформ, из которых собирается уровень")]
    [SerializeField]
    private GameObject _platformPrefab = default;
    [Tooltip("Префаб стартовой площадки, на которой появляется персонаж")]
    [SerializeField]
    private GameObject _startPlatformPrefab = default;
    [Tooltip("Стартовая позиция первой платформы, с которой будут генерироваться остальные")]
    [SerializeField]
    private Vector3 _startPosition = default;

    [Header("Дополнительные настройки")]
    [Tooltip("Сложность игры")]
    [SerializeField]
    private Difficulty _difficulty = default;

    /// <summary>
    /// Тип сложности игры
    /// Сложность определяется шириной дорожки
    /// </summary>
    public enum Difficulty
    {
        EASY = 3,
        MEDIUM = 2,
        HARD = 1
    }
    
    public bool gameOver = true;

    private int _distance;

    void OnDestroy()
    {
        EventManager.Instance.Remove(GameEventType.READY_START, RestartGame);
    }

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    #region Public

    /// <summary>
    /// Инициация игрового мира
    /// </summary>
    public void Init()
    {
        _distance = 0;
        CreateWorld();
    }

    /// <summary>
    /// Запуск уровня
    /// </summary>
    public void StartGame()
    {
        gameOver = false;
        EventManager.Instance.Dispatch(GameEventType.GAME_START);
    }
    
    /// <summary>
    /// Обработка проигрыша
    /// </summary>
    public void GameOver()
    {
        gameOver = true;
        EventManager.Instance.Dispatch(GameEventType.GAME_OVER);
        EventManager.Instance.AddOnce(GameEventType.READY_START, RestartGame);
    }
    
    /// <summary>
    /// Возвращает пройденную дистанцию
    /// </summary>
    /* на данный момент никак не используется. 
     * Реализовал просто как один из вариантов расширения игровой механики*/
    public void UpdateDistance()
    {
        _distance++;
    }
    #endregion

    #region Private
    private void RestartGame(object obj)
    {
        StopAllCoroutines();
        CreateWorld();
        _player.GetComponent<PlayerController>().Init();
        _mainCamera.transform.position = _startPosCamera;
    }

    private void CreateWorld()
    {
        PlatformSpawner.Instance.Init(_startPlatformPrefab, _platformPrefab, _itemPrefab, _startPosition, _difficulty);
    }
    #endregion

}
