﻿using UnityEngine;

/// <summary>
/// Класс, определяющий логику передвижения игрового персонажа
/// </summary>
/* По факту исопльзоваться должен только алгоритм MoveMethod.CLASSIC
 * Остальные алгоритмы в данной версии игры бесполезны. Они созданы
 * исключительно для демонстрации. При добавлении каких-либо других режимов
 * игры эти алгоритмы могут стать полезными. */
public class MoveStrategy
{
    
    delegate Vector3 changeDirectionDelegate(Vector3 currentDirection);
    private changeDirectionDelegate _delegate;

    /// <summary>
    /// Тип алгоритма передвижения игрока
    /// </summary>
    public enum MoveMethod
    {
        CLASSIC,
        CHAOTIC,
        CIRCULAR
    }
    
    public MoveStrategy(MoveMethod method)
    {
        switch(method)
        {
            case MoveMethod.CLASSIC:
                _delegate = new changeDirectionDelegate(ClassicMovement);
                break;
            case MoveMethod.CHAOTIC:
                _delegate = new changeDirectionDelegate(ChaoticMovement);
                break;
            case MoveMethod.CIRCULAR:
                _delegate = new changeDirectionDelegate(CircularMovement);
                break;
            default:
                _delegate = new changeDirectionDelegate(FakeMovement);
                break;
        }
    }

    #region Public
    /// <summary>
    /// Меняет направление движения согласно выбранному алгоритму MoveStrategy.MoveMethod
    /// </summary>
    /// <param name="currentDirection">Текущий вектор движения</param>
    /// <returns></returns>
    public Vector3 ChangeDirection(Vector3 currentDirection)
    {
        return _delegate.Invoke(currentDirection);
    }
    #endregion

    #region Private
    private Vector3 ClassicMovement(Vector3 currentDirection)
    {
        return currentDirection == Vector3.right ? Vector3.forward : Vector3.right;
    }

    private Vector3 ChaoticMovement(Vector3 currentDirection)
    {
        Vector3 direct;
        int randomPoint = Random.Range(0, 5);
        switch(randomPoint)
        {
            case 0:
                direct = Vector3.forward;
                break;
            case 1:
                direct = Vector3.right;
                break;
            case 2:
                direct = Vector3.back;
                break;
            case 3:
                direct = Vector3.left;
                break;
            default:
                direct = Vector3.zero;
                break;

        }
        return direct;
    }

    private Vector3 CircularMovement(Vector3 currentDirection)
    {
        if(currentDirection == Vector3.forward)
        {
            return Vector3.right;
        }
        else if(currentDirection == Vector3.right)
        {
            return Vector3.back;
        }
        else if (currentDirection == Vector3.back)
        {
            return Vector3.left;
        }
        else if (currentDirection == Vector3.left)
        {
            return Vector3.forward;
        }

        return Vector3.zero;
    }

    private Vector3 FakeMovement(Vector3 currentDirection)
    {
        return Vector3.zero;
    }
    #endregion

}
