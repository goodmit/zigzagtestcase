﻿using UnityEngine;

/// <summary>
/// Класс, отвечающий за плавное слежение камеры за объектом
/// </summary>
public class CameraFollow : MonoBehaviour
{
    [Tooltip("Скорость реагирования камеры на смещение цели")]
    [SerializeField]
    private float _lerpIndex = default;
    [Tooltip("Цель слежения камеры")]
    [SerializeField]
    private GameObject _target = default;

    private Vector3 _offset;
    private bool _needFollow;
    
    // Start is called before the first frame update
    void Start()
    {
        _needFollow = true;
        _offset = _target.transform.position - transform.position;
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        //_needFollow = !GameController.Instance.gameOver;
        _needFollow = !_target.GetComponent<PlayerController>().isFalling();
        if(_needFollow)
        {
            Follow();
        }
    }

    #region Private
    private void Follow()
    {
        Vector3 targetPosition = _target.transform.position - _offset;
        transform.position = Vector3.Lerp(transform.position, targetPosition, _lerpIndex * Time.deltaTime);
    }
    #endregion
}
