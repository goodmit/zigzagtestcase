﻿using UnityEngine;

/// <summary>
/// Класс, отвечающий за работу алгоритма спавна игровых объектов
/// </summary>
public class SpawnMethod
{
    delegate bool spawnMethodDelegate();
    private spawnMethodDelegate _delegate;

    private bool _spawnedAtBlock = false;
    private int _attempt = 0;
    private int _blockId = 1;
    private int _blockSize;

    /// <summary>
    /// Метод спавна
    /// </summary>
    public enum Method
    {
        RANDOM,
        ORDER5
    }

    public SpawnMethod(Method method, int blockSize)
    {
        _blockSize = blockSize;

        switch (method)
        {
            case Method.RANDOM:
                _delegate = new spawnMethodDelegate(RandomSpawn);
                break;
            case Method.ORDER5:
                _delegate = new spawnMethodDelegate(Order5Spawn);
                break;
            default:
                _delegate = new spawnMethodDelegate(RandomSpawn);
                break;
        }
    }

    #region Public
    /// <summary>
    /// Возвращает ответ, нужно ли спавнить предмет в текущем запросе
    /// </summary>
    /// <returns></returns>
    public bool IsNeedSpawn()
    {
        return _delegate.Invoke();
    }

    #endregion

    #region Private
    private bool RandomSpawn()
    {
        bool result = false;
        _attempt++;
        if(_attempt >= _blockSize)
        {
            _attempt = 0;

            result = !_spawnedAtBlock;
            _spawnedAtBlock = false;
        }
        else
        {
            if(!_spawnedAtBlock)
            {
                int chance = Random.Range(0, 4);
                if (chance < 1)
                {
                    _spawnedAtBlock = true;
                    return true;
                }
            }
        }
        return result;
    }

    private bool Order5Spawn()
    {
        bool result = false;
        _attempt++;
        
        if(_attempt == _blockId)
        {
            result = true;
        }

        if(_attempt >= _blockSize)
        {
            _attempt = 0;
            _blockId = _blockId >= _blockSize ? 1 : _blockId + 1;
        }

        return result;
    }

    #endregion

}
