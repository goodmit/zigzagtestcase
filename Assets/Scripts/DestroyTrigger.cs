﻿using JetBrains.Annotations;
using System.Collections;
using UnityEngine;

/// <summary>
/// Класс - триггер.
/// Следит за событиями различных коллизий
/// и удаляет объекты по мере надобности
/// </summary>
public class DestroyTrigger : MonoBehaviour
{
    [Tooltip("Удаляемая платформа")]
    [SerializeField]
    private GameObject _target = default;

    [Tooltip("экземпляр кристалла. добавляется динамически, если будет сгенерирован на этой платформе")]
    [CanBeNull]
    public GameObject item;

    private float _delayTime = 0.5f;
    private float _duration = 1.5f;

    void OnDestroy()
    {
        EventManager.Instance.Remove(GameEventType.GAME_OVER, OnKillMe);
        EventManager.Instance.Remove(GameEventType.REMOVE_COUPLING_ENTITY, OnIsCoupling);
    }

    void Start()
    {
        EventManager.Instance.Add(GameEventType.GAME_OVER, OnKillMe);
        EventManager.Instance.Add(GameEventType.REMOVE_COUPLING_ENTITY, OnIsCoupling);
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            EventManager.Instance.Dispatch(GameEventType.REMOVE_COUPLING_ENTITY, _target.transform.position);
            GameController.Instance.UpdateDistance();
            StartCoroutine(DestroyAnimation(_delayTime));
        }
    }


    #region Private
    private void OnKillMe(object obj)
    {
        EventManager.Instance.Remove(GameEventType.REMOVE_COUPLING_ENTITY, OnIsCoupling);
        PrepareToKill();
    }

    private void OnIsCoupling(object obj)
    {
        Vector3 passedPosition = (Vector3)obj;
        if (_target.transform.position.x + 2 < passedPosition.x || _target.transform.position.z + 2 < passedPosition.z)
        {
            EventManager.Instance.Remove(GameEventType.GAME_OVER, OnKillMe);
            PrepareToKill();
        }
    }

    IEnumerator JustDestroy(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (_target != null)
        {
            Destroy(_target);
        }
    }

    IEnumerator DestroyAnimation(float delay)
    {
        yield return new WaitForSeconds(delay);
        PrepareToKill();
        PlatformSpawner.Instance.SpawnPlatform();
    }

    private void PrepareToKill()
    {
        GetComponentInParent<Rigidbody>().useGravity = true;
        GetComponentInParent<Rigidbody>().isKinematic = false;
        StartCoroutine(JustDestroy(_duration));
        if(item != null)
        {
            Destroy(item);
        }
    }
    #endregion

}
