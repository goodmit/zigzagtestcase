﻿using UnityEngine;

/// <summary>
/// Класс, отвечающий за появление платформ и других объектов на игровом уровне
/// </summary>
public class PlatformSpawner : Singleton<PlatformSpawner>
{
    [Tooltip("Рандомизатор для выбора направления. Среднее значение примерно уравнивает шансы на выбор поворота как прямо, так и направо.")]
    [Range(0, 1)]
    public float pathVariator = 0.5f;
    [Tooltip("Алгоритм метода спавна кристаллов")]
    public SpawnMethod.Method method;
    [Tooltip("Количество платформ, которые заспавнятся с самого старта уровня")]
    public int startSpawnCount;

    private GameObject _currentPlatform;
    private GameObject _startPlatform;
    private Vector3 _startPosition;
    private GameObject _item;
    private static int _platformCount = 0;
    
    private SpawnMethod _spawnMethod;
    private Vector3 _currentPosition;
    private float _platformSizeX;
    private float _platformSizeZ;
    private int _difficultyMultiplier;


    void OnDestroy()
    {
        EventManager.Instance.Remove(GameEventType.GAME_START, StartGame);
    }

    #region Public

    /// <summary>
    /// инициализация.
    /// устанавливает дефолтное состояние игрового уровня
    /// </summary>
    /// <param name="startPlatform">префаб стартовой площадки</param>
    /// <param name="platform">префаб платформы</param>
    /// <param name="item">префаб кристалла</param>
    /// <param name="startPosition">стартовая позиция первой платформы</param>
    /// <param name="difficulty">сложность уровня</param>
    public void Init(GameObject startPlatform, GameObject platform, GameObject item, Vector3 startPosition, GameController.Difficulty difficulty)
    {
        _difficultyMultiplier = (int)difficulty;
        Vector3 newScale = new Vector3(_difficultyMultiplier, 0.5f, _difficultyMultiplier);

        EventManager.Instance.AddOnce(GameEventType.GAME_START, StartGame);

        _spawnMethod = new SpawnMethod(method, 5);
        ResetData();

        _item = item;
        _currentPosition = startPosition;
        _startPlatform = startPlatform;
        _currentPlatform = platform;
        _currentPlatform.transform.localScale = newScale;
        CreateNewPlatform(true);
    }

    /// <summary>
    /// Генерирует набор первичных платформ.
    /// </summary>
    /// <param name="obj"></param>
    public void StartGame(object obj)
    {
        for (int i = 0; i < startSpawnCount; i++)
        {
            SpawnPlatform();
        }
    }

    /// <summary>
    /// Генерирует платформу и кристалл(согласно алгоритму спавна)
    /// </summary>
    public void SpawnPlatform()
    {
        CreateNewPlatform();
        SpawnItems();
        SetNewPosition(GetDirection());
    }

    /// <summary>
    /// Возвращает количество заспавненных платформ (дополнительные платформы не в счет)
    /// </summary>
    /// <returns></returns>
    public int SpawnedPlatforms()
    {
        return _platformCount;
    }

    #endregion

    #region Private
    private Vector3 GetDirection()
    {
        float point = Random.Range(0f, 1f);
        return point < pathVariator ? Vector3.forward : Vector3.right;
    }

    private void SetNewPosition(Vector3 pos)
    {
        if(pos == Vector3.right)
        {
            _currentPosition.x += _platformSizeX;
        }
        else
        {
            _currentPosition.z += _platformSizeZ;
        }
    }

    private void CreateNewPlatform(bool firstPlatform = false)
    {
        if (firstPlatform)
        {
            _startPlatform = Instantiate(_startPlatform, Vector3.zero, Quaternion.identity);
            _startPlatform.name = "Start_platform";
        }
        else
        {
            _platformCount++;
            _currentPlatform = Instantiate(_currentPlatform, _currentPosition, Quaternion.identity);
            _platformSizeX = _currentPlatform.transform.localScale.x;
            _platformSizeZ = _currentPlatform.transform.localScale.z;

            _currentPlatform.name = "Platform_" + _platformCount + "#";
            
        }
        
    }
    
    private void ResetData()
    {
        _platformCount = 0;
    }

    private void SpawnItems()
    {
        
        if (_spawnMethod.IsNeedSpawn() && _item != null)
        {
            Vector3 itemPosition = _currentPosition;
            float offsetX, offsetZ;
            
            //создаем смещение кристалла, чтобы он не всегда появлялся ровно по центру платформы.
            offsetX = Random.Range(0, _platformSizeX);
            offsetZ = Random.Range(0, _platformSizeZ);
            //расброс должен быть как в одну, так и в другую сторону
            //поэтому смещаем ренж с 0...1 до -0.5...+0.5
            offsetX -= _platformSizeX / 2;
            offsetZ -= _platformSizeZ / 2;
            itemPosition.x += offsetX;
            itemPosition.z -= offsetZ;
            
            _item = Instantiate(_item, itemPosition, Quaternion.identity);
            
            _currentPlatform.GetComponentInChildren<DestroyTrigger>().item = _item;
        }
    }
    #endregion

}
