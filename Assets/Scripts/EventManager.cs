﻿using System.Collections.Generic;
using System;
using JetBrains.Annotations;
using UnityEngine;


/// <summary>
/// Класс, описывающий одиночное базовое событие
/// </summary>
public class BaseEvent {
    /// <summary>
    /// Тип события
    /// </summary>
    public GameEventType EventName { get; set; }
    
    /// <summary>
    /// Слушатель события
    /// </summary>
    public Action<object> ListenerAction { get; set; }

    /// <summary>
    /// Флажок "одноразовости" события: если true, то событие автоматически удалится из стэка сразу после вызова
    /// </summary>
    public Boolean DeleteAfterCall { get; set; }

    /// <summary>
    /// Метод, который дополнительно запустится после наступления события
    /// </summary>
    [CanBeNull]
    public Action CallerAction { get; set; }
}

/// <summary>
/// Типы игровых событий
/// </summary>
public enum GameEventType {
    REMOVE_COUPLING_ENTITY,
    GAME_START,
    GAME_PAUSE,
    GAME_OVER,
    READY_START
}

/// <summary>
/// Менеджер событий
/// Класс, отвечающий за хранение и обработку игровых событий на протяжении все игровой сессии
/// </summary>
public class EventManager : Singleton<EventManager>
{
    
    private readonly Dictionary<GameEventType, List<BaseEvent>> mEventDictionary = new Dictionary<GameEventType, List<BaseEvent>>();

    #region Public Methods
    /// <summary>
    /// Добавляет бессрочно слушатель на событие в стэк. Слушатель не удаляется из стэка после вызова события
    /// </summary>
    /// <param name="uiEvent">тип события</param>
    /// <param name="listenerAction">слушатель события</param>
    /// <param name="callerAction">дополнительный метод, запускащийся при наступлении события</param>
    public void Add(GameEventType uiEvent, Action<object> listenerAction, Action callerAction = null)
    {
        AddHandler(uiEvent, listenerAction, false, callerAction);
    }

    /// <summary>
    /// Единоразово добавляет слушатель на событие в стэк. Слушатель удалится из стэка после первого же вызова события
    /// </summary>
    /// <param name="uiEvent">тип события</param>
    /// <param name="listenerAction">слушатель события</param>
    /// <param name="callerAction"></param>
    public void AddOnce(GameEventType uiEvent, Action<object> listenerAction, Action callerAction = null)
    {
        AddHandler(uiEvent, listenerAction, true, callerAction);
    }

    /// <summary>
    /// Отправляет произошедшее событие менеджеру событий.
    /// Срабатывают все слушатели, подписанные на данное событие.
    /// </summary>
    /// <param name="uiEvent">тип события</param>
    /// <param name="obj">дополнительный параметр-аргумент для слушателя события</param>
    public void Dispatch(GameEventType uiEvent, object obj = null)
    {
        if (!mEventDictionary.ContainsKey(uiEvent))
        {
            //Debug.Log(uiEvent + " Событие не существует !!");
            return;
        }

        List<Action<object>> prepareToRemove = new List<Action<object>>();

        foreach (var @event in mEventDictionary[uiEvent])
        {
            @event.CallerAction?.Invoke();
            @event.ListenerAction(obj);
            if (@event.DeleteAfterCall)
            {
                //Debug.Log("слушатель нужно удалить!");
                prepareToRemove.Add(@event.ListenerAction);
            }
        }
        for (int i = prepareToRemove.Count - 1; i >= 0; i--)
        {
            Remove(uiEvent, prepareToRemove[i]);
            prepareToRemove.RemoveAt(i);
        }
    }

    /// <summary>
    /// Удаляет все слушатели указанного события из стэка, если они там имеются
    /// </summary>
    /// <param name="uiEvent">тип события</param>
    /// <param name="listenerAction">слушатель данного события</param>
    public void Remove(GameEventType uiEvent, Action<object> listenerAction)
    {
        if (!mEventDictionary.ContainsKey(uiEvent))
        {
            //Debug.Log(uiEvent + " Событие не существует !!");
            return;
        }

        for (int i = mEventDictionary[uiEvent].Count - 1; i >= 0; i--)
        {
            if (mEventDictionary[uiEvent][i].ListenerAction.Equals(listenerAction))
            {
                mEventDictionary[uiEvent].RemoveAt(i);
                //Debug.Log(uiEvent + " Событие удалено !!");
            }
        }
    }
    #endregion

    #region Private Methods
    private void AddHandler(GameEventType uiEvent, Action<object> listenerAction, Boolean deleteAfterCall, Action callerAction = null)
    {
        var newUIEvent = new BaseEvent
        {
            EventName = uiEvent,
            ListenerAction = listenerAction,
            DeleteAfterCall = deleteAfterCall,
            CallerAction = callerAction
        };
        if (!mEventDictionary.ContainsKey(uiEvent))
        {
            var newBaseTimerList = new List<BaseEvent>();
            mEventDictionary.Add(uiEvent, newBaseTimerList);
        }
        mEventDictionary[uiEvent].Add(newUIEvent);
    }
    #endregion
}
