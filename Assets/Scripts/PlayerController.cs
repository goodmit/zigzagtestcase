﻿using UnityEngine;

/// <summary>
/// Класс, отвечающий за управление игровым персонажем
/// </summary>
public class PlayerController : MonoBehaviour
{
    [Tooltip("Начальная скорость игрового персонажа")]
    public float startSpeed;
    [Tooltip("Слой-метка для определения коллизий с дорожкой")]
    public LayerMask groundLayer;
    [Tooltip("Тип алгоритма движения игрового персонажа")]
    public MoveStrategy.MoveMethod _method;

    private float _currentSpeed;
    private float _fallSpeed = 20f;
    private bool _started;
    private bool _falling;
    private bool _grounded = true;
    private bool _gameOver;
    private Vector3 _direction;
    private MoveStrategy _strategy;
    private Rigidbody _rigidBody;

    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _strategy = new MoveStrategy(_method);
    }

    // Start is called before the first frame update
    void Start()
    {
        name = "Player";
        Init();
    }

    // Update is called once per frame
    /* тут чертовщина. далеко не самый лучший вариант реализации. но так получилось*/
    void Update()
    {
        if (_gameOver) return;
        if(!_grounded)
        {
            if (!_falling)
            {
                _falling = true;
                Invoke("SendGameOver", 0.8f);
            }
            Movement();   
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (!_started)
            {
                _currentSpeed = startSpeed;
                _started = true;
                GameController.Instance.StartGame();
            } else
            {
                SwitchDirection();
            }
            
        }

        if (!_started) return;

        Movement();
        _grounded = Physics.Raycast(transform.position, Vector3.down, 5, groundLayer);
        if(!_grounded)
        {
            _direction = Vector3.down;
            _currentSpeed = _fallSpeed;
        }
    }

    /// <summary>
    /// Инициализация игрового персонажа.
    /// Подготавливает персонажа для старта уровня.
    /// </summary>
    public void Init()
    {
        _currentSpeed = 0;
        _rigidBody.velocity = Vector3.zero;
        _direction = Vector3.right;
        gameObject.transform.position = Vector3.zero;
        _started = false;
        _grounded = true;
        _gameOver = false;
        _falling = false;
    }

    #region Public
    /// <summary>
    /// Возвращает ответ, находится ли персонажа в состоянии свободного падения
    /// </summary>
    /// <returns></returns>
    public bool isFalling()
    {
        return _falling;
    }
    #endregion

    #region Private
    private void SendGameOver()
    {
        _gameOver = true;
        GameController.Instance.GameOver();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            other.gameObject.GetComponent<IInteractable>().Interact();
        }
    }

    private void Movement()
    {
        _rigidBody.velocity = _direction * _currentSpeed;
    }

    private void SwitchDirection()
    {
        _direction = _strategy.ChangeDirection(_direction);
    }
    #endregion

}
