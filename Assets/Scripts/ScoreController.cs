﻿using UnityEngine;

/// <summary>
/// Контроллер игрового счета
/// Отвечает за логику начисления/хранения игровых очков, набранных игроком
/// </summary>
public class ScoreController : Singleton<ScoreController>
{

    private int _score;
    private int _hiscore;

    void OnDestroy()
    {
        EventManager.Instance.Remove(GameEventType.GAME_START, ResetScore);
        EventManager.Instance.Remove(GameEventType.GAME_OVER, GameOver);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        EventManager.Instance.Add(GameEventType.GAME_START, ResetScore);
        EventManager.Instance.Add(GameEventType.GAME_OVER, GameOver);
        _score = 0;
        LoadData();
    }

    #region Public
    /// <summary>
    /// Добавляет игровые очки
    /// </summary>
    /// <param name="value">Количество очков</param>
    public void AddScore(int value)
    {
        _score += value;
    }

    /// <summary>
    /// Обновляет статистику по игровым очкам и рекордам
    /// </summary>
    /// <param name="obj"></param>
    public void GameOver(object obj)
    {
        if(_score > _hiscore)
        {
            _hiscore = _score;
            SaveHiscore();
        }
    }

    /// <summary>
    /// Возвращает текущее количество очков
    /// </summary>
    /// <returns></returns>
    public int Score()
    {
        return _score;
    }

    /// <summary>
    /// Возвращает рекордное количество набранных очков
    /// </summary>
    /// <returns></returns>
    public int Hiscore()
    {
        return _hiscore;
    }

    /// <summary>
    /// Сбрасывает текущие очки до нуля
    /// </summary>
    /// <param name="obj"></param>
    public void ResetScore(object obj)
    {
        _score = 0;
    }

    /// <summary>
    /// Сохраняет лучший счет
    /// </summary>
    public void SaveHiscore()
    {
        PlayerPrefs.SetInt("hiscore", _hiscore);
    }
    #endregion

    #region Private
    private void LoadData()
    {
        _hiscore = PlayerPrefs.GetInt("hiscore", 0);
    }
    #endregion
}
