﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Контроллер интерфейсов.
/// Клас, отвечающий за поведение интерфейсных элементов в игре
/// </summary>
public class UIController : Singleton<UIController>
{
    private static float DELAY_TIME = 2f;
        
    [Tooltip("Стартовая панель")]
    [SerializeField]
    private GameObject _startPanel = default;
    [Tooltip("Панель при проигрыше")]
    [SerializeField]
    private GameObject _gameOverPanel = default;
    [Tooltip("Отображает количество набранных очков")]
    [SerializeField]
    private Text _scoreTxt = default;
    [Tooltip("Отображает лучший счет")]
    [SerializeField]
    private Text _hiscoreTxt = default;
    
    void OnDestroy()
    {
        EventManager.Instance.Remove(GameEventType.GAME_START, HidePanels);
        EventManager.Instance.Remove(GameEventType.GAME_OVER, UpdateData);
    }

    // Start is called before the first frame update
    void Start()
    {
        EventManager.Instance.Add(GameEventType.GAME_START, HidePanels);
        EventManager.Instance.Add(GameEventType.GAME_OVER, UpdateData);
        _startPanel.SetActive(true);
        _gameOverPanel.SetActive(false);
    }

    #region Public

    /// <summary>
    /// Скрывает все панели с экрана
    /// </summary>
    /// <param name="obj"></param>
    public void HidePanels(object obj)
    {
        _startPanel.SetActive(false);
        _gameOverPanel.SetActive(false);
    }

    /// <summary>
    /// Обновляет отображение на панелях
    /// </summary>
    /// <param name="obj"></param>
    public void UpdateData(object obj)
    {
        _scoreTxt.text = "Твой счёт: " + ScoreController.Instance.Score().ToString();
        _hiscoreTxt.text = "Лучший счёт: " + ScoreController.Instance.Hiscore().ToString();
        _gameOverPanel.SetActive(true);
        StartCoroutine(ShowStartPanel());
    }

    /// <summary>
    /// Выход из игры
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }

    #endregion

    #region Private
    private IEnumerator ShowStartPanel()
    {
        yield return new WaitForSeconds(DELAY_TIME);
        EventManager.Instance.Dispatch(GameEventType.READY_START);
        _startPanel.SetActive(true);
    }
    #endregion

}
